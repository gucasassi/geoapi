package es.ivks.geoapi.data.bootstrap;

import es.ivks.geoapi.business.model.Comunidad;
import es.ivks.geoapi.data.entities.ComunidadEntity;
import es.ivks.geoapi.data.repository.ComunidadRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author: guillem.casas
 * @version: 10/02/2021
**/
@Slf4j
@Component
public class DataLoader implements CommandLineRunner {

    private final ComunidadRepository comunidadRepository;

    public DataLoader(ComunidadRepository comunidadRepository) {
        this.comunidadRepository = comunidadRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        if(comunidadRepository.count() == 0){
            loadComunidades();
            log.info("Loaded Communities: " + comunidadRepository.count());
        }

    }

    private void loadComunidades() {

        ComunidadEntity catalunya = ComunidadEntity.builder().name("Catalunya").build();
        ComunidadEntity euskadi = ComunidadEntity.builder().name("Euskadi").build();

        comunidadRepository.save(catalunya);
        comunidadRepository.save(euskadi);

    }

}
