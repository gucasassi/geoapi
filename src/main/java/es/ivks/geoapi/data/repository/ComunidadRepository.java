package es.ivks.geoapi.data.repository;

import es.ivks.geoapi.data.entities.ComunidadEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author: guillem.casas
 * @version: 10/02/2021
 **/
public interface ComunidadRepository extends PagingAndSortingRepository<ComunidadEntity, Long> {
}
