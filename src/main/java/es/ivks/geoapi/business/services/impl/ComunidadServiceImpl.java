package es.ivks.geoapi.business.services.impl;

import es.ivks.geoapi.business.mappers.ComunidadMapper;
import es.ivks.geoapi.business.model.Comunidad;
import es.ivks.geoapi.business.services.ComunidadService;
import es.ivks.geoapi.data.entities.ComunidadEntity;
import es.ivks.geoapi.data.repository.ComunidadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister.*;
import org.springframework.stereotype.Service;

/**
 * @author: guillem.casas
 * @version: 09/02/2021
**/
@Service
@RequiredArgsConstructor
public class ComunidadServiceImpl implements ComunidadService {

    private final ComunidadMapper mapper;
    private final ComunidadRepository repository;

    @Override
    public Comunidad getComunidadById(Long id) throws NotFoundException {

        return mapper.comunidadEntityToComunidad(
                repository.findById(id).orElseThrow(NotFoundException::new)
        );

    }

    @Override
    public Comunidad saveComunidad(Comunidad comunidad) {

        ComunidadEntity comunidadEntity = mapper.comunidadToComunidadEntity(comunidad);

        return mapper.comunidadEntityToComunidad(
                repository.save(comunidadEntity)
        );

    }

    @Override
    public void updateComunidad(Long id, Comunidad comunidad) throws NotFoundException {

        ComunidadEntity comunidadEntity = repository.findById(id).orElseThrow(NotFoundException::new);
        comunidadEntity.setName(comunidad.getName());

        repository.save(comunidadEntity);

    }

    @Override
    public void deleteComunidad(Long id) throws NotFoundException {

        ComunidadEntity comunidadEntity = repository.findById(id).orElseThrow(NotFoundException::new);
        repository.delete(comunidadEntity);

    }

}
